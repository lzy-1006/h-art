import numpy as np
import h5py


class UniformOctree:

    def __init__(self, boxsize, n_ref):

        Nleaf = 8**n_ref
        Nparent = 0
        for n in range(n_ref):
            Nparent += 8**n
        Ncell = Nparent + Nleaf

        min_x = np.zeros((Ncell, 3), dtype=np.float64)
        width = np.zeros(Ncell, dtype=np.float64)
        parent_ID = np.zeros(Ncell, dtype=np.int64)
        sub_cell_check = np.zeros(Ncell, dtype=np.int32)
        sub_cell_IDs = np.zeros((Nparent, 8), dtype=np.int64)

        # assign parent/children IDs
        for n in range(n_ref):
            parent_start_id = 0
            for i in range(n):
                parent_start_id += 8**i
            child_start_id = parent_start_id + 8**n

            # loop over parents in level n
            for j in range(8**n):
                i_parent = parent_start_id + j
                i_child = child_start_id + 8 * j
                sub_cell_check[i_parent] = 1
                # assign children to parent
                for k in range(8):
                    sub_cell_IDs[i_parent, k] = i_child + k
                # assign parent to children
                for k in range(8):
                    parent_ID[i_child + k] = i_parent

        parent_ID[0] = -1

        # assign coordinates and width
        min_x[0, 0] = -boxsize / 2
        min_x[0, 1] = -boxsize / 2
        min_x[0, 2] = -boxsize / 2
        width[0] = boxsize
        for n in range(n_ref):
            parent_start_id = 0
            for i in range(n):
                parent_start_id += 8**i

            for j in range(8**n):
                i_parent = parent_start_id + j
                for nx in range(2):
                    for ny in range(2):
                        for nz in range(2):
                            k = nx * 4 + ny * 2 + nz
                            width[sub_cell_IDs[i_parent, k]
                                  ] = width[i_parent] / 2
                            min_x[sub_cell_IDs[i_parent, k],
                                  0] = min_x[i_parent, 0] + nx * width[i_parent] / 2
                            min_x[sub_cell_IDs[i_parent, k],
                                  1] = min_x[i_parent, 1] + ny * width[i_parent] / 2
                            min_x[sub_cell_IDs[i_parent, k],
                                  2] = min_x[i_parent, 2] + nz * width[i_parent] / 2

        self.Ncell = Ncell
        self.Nparent = Nparent
        self.Nleaf = Nleaf
        self.boxsize = boxsize
        self.min_x = min_x
        self.width = width
        self.parent_ID = parent_ID
        self.sub_cell_check = sub_cell_check
        self.sub_cell_IDs = sub_cell_IDs

        return


def randtree(boxsize, n_ref, rho, T, fname):

    otree = UniformOctree(boxsize, n_ref)
    otree.rho = rho * (np.ones(otree.Nleaf, dtype=np.float64) + 0.1 *
                       np.random.randn(otree.Nleaf))
    otree.nh = 0.5 * np.ones(otree.Nleaf, dtype=np.float64) + 0.1 * \
        np.random.randn(otree.Nleaf)
    otree.ne = 0.5 * np.ones(otree.Nleaf, dtype=np.float64) + 0.1 * \
        np.random.randn(otree.Nleaf)
    otree.z = 0.01 * np.ones(otree.Nleaf, dtype=np.float64) + 0.001 * \
        np.random.randn(otree.Nleaf)
    otree.T = T * (np.ones(otree.Nleaf, dtype=np.float64) +
                   0.1 * np.random.randn(otree.Nleaf))
    otree.vel = np.random.randn(otree.Nleaf, 3)
    # otree.vel[:,2] = 1.0 # set unit to 0 if static

    f = h5py.File(fname + '.hdf5', 'w')
    f.attrs['Ncell'] = otree.Ncell
    f.attrs['Nparent'] = otree.Nparent
    f.attrs['Nleaf'] = otree.Nleaf
    f.attrs['Boxsize'] = otree.boxsize / 2  # half size
    f.create_dataset('min_x', data=otree.min_x)
    f.create_dataset('width', data=otree.width)
    f.create_dataset('parent_ID', data=otree.parent_ID)
    f.create_dataset('sub_cell_check', data=otree.sub_cell_check)
    f.create_dataset('sub_cell_IDs', data=otree.sub_cell_IDs)
    f.create_dataset('vel', data=otree.vel)
    f.create_dataset('rho', data=otree.rho)
    f.create_dataset('nh', data=otree.nh)
    f.create_dataset('ne', data=otree.ne)
    f.create_dataset('z', data=otree.z)
    f.create_dataset('T', data=otree.T)
    f.close()

    return


# create a randomised tree for Stromgren test
n = 0.1
T = 1e4
L = 0.2
n_ref = 7
kpc = 3.086e21
mp = 1.6726219e-24
Msun = 1.989e33

fname = "randtree_L%.1f_n%.1f" % (L, n)

randtree(L, n_ref, mp * n * (kpc**3 / (1e10 * Msun)), T, fname)
