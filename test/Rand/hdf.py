import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import h5py

mplstyle.use('fast')

#fname = input('HDF5 file name: ')
fname = 'photons_000.h5'
f = h5py.File(fname, 'r')
print('Keys are ', f.keys())
print(f.items())

fat = list(f['fate'][:])
print(fat.count(-1), 'absorbed')
print(fat.count(1), 'escaped')

freq = list(f['nu'][:])
bet = 0
for n in freq:
    if n > 5.5e14:
        bet += 1
print(bet * 100 / len(freq), '% are H-beta')

pos = f['pos']
# Coordinates in 3D
coor = [[j[i] for j in pos]
        for i in range(pos.shape[1])]  # i is spatial dimension
print('Coordinate parsing complete')

fig = plt.figure()

dist = fig.add_subplot(221, projection='3d')
dist.scatter(coor[0], coor[1], coor[2], c=freq)
dist.set_title('Spatial photon distribution')

spec = fig.add_subplot(222)
spec.hist(freq)
spec.set_title('Spectrum')

plt.show()
