# parameter file

OctreeFile					h-art/test/stromgren/ICs/unitree_L0.2_n0.1.hdf5
OutputDir					h-art/test/stromgren
HydrogenFraction			1

NphotonCell				1000
NphotonPointSource			0
NphotonGeometry				0

UnitMass_in_g				1
UnitLength_in_cm			1
UnitVelocity_in_cm_per_s	1e5
HubbleParam                 0

DustOpacity				1.158e5
DustToMetalRatio			0.4
DustAlbedo				0.3394

NumberOfCameras				0
CameraInfoFile				h-art/src/camera.txt