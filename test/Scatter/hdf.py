import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import h5py

mplstyle.use('fast')

#fname = input('HDF5 file name: ')
fname = 'photons_000.h5'
f = h5py.File(fname, 'r')
print('Keys are ', f.keys())
print(f.items())
pos = f['pos']
freq = list(f['nu'])
fat = list(f['fate'])
nha = list(f['n_hat'])

sca = 0
for n in nha:
    if n[0] != 1 and n[1] != 0:
        print('Scattered photon', sca, 'has direction', n)
        sca += 1

abs = fat.count(-1)
print(abs, 'absorbed')
esc = fat.count(1)
print(esc, 'escaped')
print('This corresponds to an average optical depth of', -np.log(esc / (abs + esc)))
bet = 0
for n in freq:
    if n > 5.5e14:
        bet += 1
print(bet, ' among all are H-beta')

# Coordinates in 3D
coor = [[j[i] for j in pos]
        for i in range(pos.shape[1])]  # i is spatial dimension

fig = plt.figure()

dist = fig.add_subplot(211, projection='3d')
dist.scatter(coor[0], coor[1], coor[2])
dist.set_title('Spatial photon distribution')

spec = fig.add_subplot(212)
spec.hist(freq)
spec.set_title('Spectrum')

plt.show()
