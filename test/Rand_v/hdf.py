import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import h5py

mplstyle.use('fast')

#fname = input('HDF5 file name: ')
fname = 'photons_000.h5'
f = h5py.File(fname, 'r')
print('Keys are ', f.keys())
print(f.items())
pos = f['pos']
freq = f['nu']
print(freq[0:100:10])

#Coordinates in 3D
coor = [[j[i] for j in pos] for i in range(pos.shape[1])] # i is spatial dimension

fig = plt.figure()

dist = fig.add_subplot(211, projection = '3d')
dist.scatter(coor[0], coor[1], coor[2])
dist.set_title('Spatial photon distribution')

spec = fig.add_subplot(212)
spec.hist(freq)
spec.set_title('Spectrum')

plt.show()
