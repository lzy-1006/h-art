#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <omp.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "allvar.h"
#include "proto.h"


double lorentz_profile(double nu)
{
    return pc.nuL/2/pc.pi/((nu-pc.nua)*(nu-pc.nua)+pc.nuL*pc.nuL/4);
}


double voigt_profile(double x, double a)
{
    double xsq = x*x;
    double z = (xsq-0.855)/(xsq+3.42);
    double q = 0;
    if (z>0)
        q = z*(1+21/xsq)*(a/pc.pi/(xsq+1))*(0.1117+z*(4.421+z*(-9.207+5.674*z)));
    // phi(x) is normalized to 1
    return q + exp(-xsq)/pc.sqrt_pi;
}


double cw(double a)
{
    return 6.9184721 + 81.766279/(log10(a)-14.651253);
}


double set_u0(double x, double a)
{
    double xcw = cw(a);
    double u0;
    if (x<xcw)
        u0 = x - 1/(x+exp(1-x*x)/a);
    else
        u0 = xcw - 1/xcw + 0.15*(x-xcw);
    return u0;
}


double single_rejection(double x, double a)
{
    int accept = 0;
    double u;
    while (accept==0)
    {
        double r1 = gsl_rng_uniform(random_generator);
        double r2 = gsl_rng_uniform(random_generator);
        double th = pc.pi*(r1-0.5);
        u = x + a*tan(th);
        if (r2<exp(-u*u)) accept = 1;
    }
    return u;
}


double piecewise_rejection(double x, double a)
{
    double u, th;
    double u0 = set_u0(x, a);
    double eu0 = exp(-u0*u0);
    double th0 = atan((u0-x)/a);
    double p0 = (th0+pc.pi/2)/((1-eu0)*th0+(1+eu0)*pc.pi/2);

    int accept = 0;
    while (accept==0)
    {
        double r1 = gsl_rng_uniform(random_generator);
        double r2 = gsl_rng_uniform(random_generator);
        double r3 = gsl_rng_uniform(random_generator);
        if (r1<p0)
        {
            th = -pc.pi/2 + r2*(th0+pc.pi/2);
            u = x + a*tan(th);
            if (r3<exp(-u*u)) accept = 1;
        }
        else
        {
            th = th0 + r2*(pc.pi/2-th0);
            u = x + a*tan(th);
            if (r3<exp(-u*u)/eu0) accept = 1;
        }
    }
    return u;
}


double gaussian(double x)
{
    double r = gsl_ran_gaussian(random_generator, 1);
    double u = r/pc.sqrt2 + 1/x;
    return u;
}


double sample_uz(double x, double a)
{
    double u, sgn=1;
    if (x<0) {sgn = -1; x = -x;}

    if (x<=1)
        u = single_rejection(x,a);
    else if (x<=9)
        u = piecewise_rejection(x,a);
    else
        u = gaussian(x);

    return sgn*u;
}


void sample_uatom(double x, double a, double xcrit, double* u_atom)
{
    double r1 = gsl_rng_uniform(random_generator);
    double r2 = gsl_rng_uniform(random_generator);
    u_atom[0] = sqrt(xcrit*xcrit-log(r1))*cos(2*pc.pi*r2);
    u_atom[1] = sqrt(xcrit*xcrit-log(r1))*sin(2*pc.pi*r2);
    u_atom[2] = sample_uz(x,a);
}
