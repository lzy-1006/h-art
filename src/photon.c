#include <hdf5.h>
#include <hdf5_hl.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "allvar.h"
#include "proto.h"

void write_photons(void) {
  long ray_num, n_rays;
  n_rays = All.NphotonCell / NTask + All.NphotonPointSource / NTask +
           All.NphotonGeometry / NTask; // number of photons

  PHOTON *ps;
  if (!(ps = malloc(n_rays * sizeof(PHOTON)))) {
    if (ThisTask == 0)
      printf("Failed to allocate memory (PHOTON).\n");
    endrun(4);
  }

  char infile[200];
  sprintf(infile, "%s/photons_%03d.dat", All.OutputDir, ThisTask);
  FILE *fp = fopen(infile, "rb");

  // fill in photons
  for (ray_num = 0; ray_num < n_rays; ray_num++)
    fread(&ps[ray_num], sizeof(PHOTON), 1, fp);

  fclose(fp);

  // write to hdf5 file
  char outfile[200];
  sprintf(outfile, "%s/photons_%03d.h5", All.OutputDir, ThisTask);
  if (ThisTask == 0) {
    printf("------------------------------------\n\n");
    printf("# Writing photons to file %s\n\n", outfile);
  }

  hid_t file_id = H5Fcreate(outfile, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  herr_t status;

  // write datasets
  int i, *buf_int;
  double *buf_double;

  buf_double = malloc(n_rays * 3 * sizeof(double));
  for (ray_num = 0; ray_num < n_rays; ray_num++)
    for (i = 0; i < 3; i++)
      buf_double[3 * ray_num + i] = ps[ray_num].pos[i];
  hsize_t dims2[2] = {n_rays, 3};
  status = H5LTmake_dataset_double(file_id, "pos", 2, dims2, buf_double);
  free(buf_double); // pos

  buf_double = malloc(n_rays * 3 * sizeof(double));
  for (ray_num = 0; ray_num < n_rays; ray_num++)
    for (i = 0; i < 3; i++)
      buf_double[3 * ray_num + i] = ps[ray_num].n_hat[i];
  status = H5LTmake_dataset_double(file_id, "n_hat", 2, dims2, buf_double);
  free(buf_double); // n_hat

  buf_double = malloc(n_rays * sizeof(double));
  for (ray_num = 0; ray_num < n_rays; ray_num++)
    buf_double[ray_num] = ps[ray_num].nu;
  hsize_t dims1[1] = {n_rays};
  status = H5LTmake_dataset_double(file_id, "nu", 1, dims1, buf_double);
  free(buf_double); // nu

  buf_double = malloc(n_rays * sizeof(double));
  for (ray_num = 0; ray_num < n_rays; ray_num++)
    buf_double[ray_num] = ps[ray_num].emis;
  status = H5LTmake_dataset_double(file_id, "emis", 1, dims1, buf_double);
  free(buf_double); // emis

  buf_int = malloc(n_rays * sizeof(int));
  for (ray_num = 0; ray_num < n_rays; ray_num++)
    buf_int[ray_num] = ps[ray_num].fate;
  status = H5LTmake_dataset_int(file_id, "fate", 1, dims1, buf_int);
  free(buf_int); // fate

  H5Fclose(file_id);

  // finish up
  remove(infile);
  free(ps);
  if (ThisTask == 0)
    printf("Done.\n\n");
}
