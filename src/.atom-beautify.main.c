#include <gsl/gsl_rng.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
//#include <omp.h>

#include "allvar.h"
#include "proto.h"

int main(int argc, char **argv) {
  // initialize MPI
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &ThisTask);
  MPI_Comm_size(MPI_COMM_WORLD, &NTask);

  if (argc < 2) {
    if (ThisTask == 0)
      printf("Usage: H-aRT <ParamFile>\n");
    endrun(1);
  }

  if (ThisTask == 0) {
    printf("------------------------------------\n\n");
    printf("# Running Balmer Radiative Transfer\n\n");
    printf("Using %d MPI Tasks\n\n", NTask);
  }

  // random number generator
  random_generator = gsl_rng_alloc(gsl_rng_ranlxd1);
  unsigned long gsl_rng_default_seed = (unsigned int)time(NULL) + ThisTask;
  gsl_rng_set(random_generator, gsl_rng_default_seed);

  // read parameter file
  char ParamFile[200];
  strcpy(ParamFile, argv[1]);
  read_parameter_file(ParamFile);
  mkdir(All.OutputDir, 02755);

  // set up constants
  fill_in_physical_constant();

  // set up cameras
  if (All.NumberOfCameras > 0)
    initialize_camera();

  // read octree
  read_octree_from_hdf5(All.OctreeFile);

  // run transport
  run_mcrt();

  // reduce and write images
  if (All.NumberOfCameras > 0) {
    reduce_images();
    write_camera_output();
  }

  // clean up
  free_octree();
  // write_photons();

  // finalize
  MPI_Finalize();
  return 0;
}

void endrun(int ierr) {
  if (ThisTask == 0)
    printf("\nEndrun called with an error level of %d\n\n", ierr);

  MPI_Finalize();
  exit(0);
}
