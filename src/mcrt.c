#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//#include <omp.h>

#include "allvar.h"
#include "proto.h"

void run_mcrt() {
  time_t start_tp, end_tp;
  float time_wasted;
  long ray_num, n_rays;

  // output file
  char fname[200];
  // sprintf(fname, "%s/photons_%03d.dat", All.OutputDir, ThisTask);
  sprintf(fname, "%s/camphotons_%03d.dat", All.OutputDir, ThisTask);
  FILE *fp = fopen(fname, "wb");

  // photon emitted from gas
  if (All.NphotonCell > 0) {
    if (ThisTask == 0) {
      printf("------------------------------------\n\n");
      printf("Total %ld photons from cells\n", All.NphotonCell);
      printf("Total %ld photons per task\n\n", All.NphotonCell / NTask);
      time(&start_tp);
    }

    // initialize
    init_emission_from_cell();

    // do transport
    n_rays = All.NphotonCell / NTask;
    int print_interval = (int)(n_rays / log(n_rays));
    for (ray_num = 0; ray_num < n_rays; ray_num++) {
      PHOTON p;
      if (ray_num % print_interval == 0) {
        printf("Photon %ld \n", ray_num);
      }
      emit_from_cell(&p, fp); // photon emission
      transport(&p, fp);      // photon transport
                         // fwrite(&p, sizeof(PHOTON), 1, fp); // write to file
    }

    if (ThisTask == 0) {
      time(&end_tp);
      time_wasted = difftime(end_tp, start_tp) / 60;
      printf("Time cost: %.2f minutes.\n\n", time_wasted);
    }
  }

  // // photon emitted from point sources
  // if (All.NphotonPointSource>0)
  // {
  //     if (ThisTask==0)
  //     {
  //         printf("------------------------------------\n\n");
  //         printf("Total %ld photons from point sources\n",
  //         All.NphotonPointSource); printf("Total %ld photons per task\n\n",
  //         All.NphotonPointSource/NTask); time(&start_tp);
  //     }
  //
  //     // initialize
  //     init_emission_from_point_source();
  //
  //     // do transport
  //     n_rays = All.NphotonPointSource/NTask;
  //     for (ray_num=0; ray_num<n_rays; ray_num++)
  //     {
  //         PHOTON p;
  //         emit_from_point_source(&p); // photon emission
  //         transport(&p); // photon transport
  //         fwrite(&p, sizeof(PHOTON), 1, fp); // write to file
  //     }
  //
  //     if (ThisTask==0)
  //     {
  //         time(&end_tp);
  //         time_wasted=difftime(end_tp,start_tp)/60;
  //         printf("Time costed: %.2f minutes.\n\n", time_wasted);
  //     }
  // }

  fclose(fp);
}
