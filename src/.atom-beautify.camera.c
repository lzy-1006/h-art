#include <hdf5.h>
#include <hdf5_hl.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "allvar.h"
#include "proto.h"

void initialize_camera() {
  if (ThisTask == 0) {
    printf("------------------------------------\n\n");
    printf("# Reading camera info from file %s\n\n", All.CameraInfoFile);
    printf("Camera Theta Phi Psi Length Npixel\n");
  }

  // allocate memory for camera
  cams = malloc(All.NumberOfCameras * sizeof(CAMERA));

  // open camera info file
  FILE *fp;
  if (!(fp = fopen(All.CameraInfoFile, "r"))) {
    if (ThisTask == 0)
      printf("Fatal error: cannot open camera info file %s.\n",
             All.CameraInfoFile);
    endrun(2);
  }

  // initialize camera
  int i;
  for (i = 0; i < All.NumberOfCameras; i++) {
    fscanf(fp, "%lf %lf %lf %lf %d\n", &cams[i].theta, &cams[i].phi,
           &cams[i].psi, &cams[i].L, &cams[i].N);
    if (ThisTask == 0)
      printf("%d %.3f %.3f %.3f %.3g %d\n", i, cams[i].theta, cams[i].phi,
             cams[i].psi, cams[i].L, cams[i].N);

    cams[i].dx = 2 * cams[i].L / cams[i].N; // pixel size
    cams[i].n_hat[0] = sin(cams[i].theta) * cos(cams[i].phi);
    cams[i].n_hat[1] = sin(cams[i].theta) * sin(cams[i].phi);
    cams[i].n_hat[2] = cos(cams[i].theta);
    cams[i].image = malloc(cams[i].N * cams[i].N * sizeof(double *));
  }

  if (ThisTask == 0)
    printf("\n");
  fclose(fp);
}

void send_photon_to_camera(PHOTON *p, double p_dipole, FILE *fp) {
  int i, j;
  for (i = 0; i < All.NumberOfCameras; i++) {
    // probability of scatter
    double mu = DOT(p->n_hat, cams[i].n_hat);
    double p_scatter = p_dipole * 0.75 * (1 + pow(mu, 2)) + (1 - p_dipole);

    // outgoing frequency
    double v_bulk[3], n_diff[3];
    for (j = 0; j < 3; j++) {
      v_bulk[j] = CELL[p->cell_id].U->vel[j] + All.HubbleParam * p->pos[j];
      n_diff[j] = cams[i].n_hat[j] - p->n_hat[j];
    }
    double nu = p->nu + (p->nu / pc.c) * DOT(v_bulk, n_diff);

    // create outgoing photon
    PHOTON p_copy;
    p_copy.nu = nu;
    p_copy.fate = ACTIVE;
    p_copy.cell_id = p->cell_id;
    for (j = 0; j < 3; j++) {
      p_copy.pos[j] = p->pos[j];
      p_copy.n_hat[j] = cams[i].n_hat[j];
    }

    // get the optical depth
    double tau = integrate_optical_depth(&p_copy);

    // flux to deposit
    double flux = (pc.h * nu * p->emis) * p_scatter * exp(-tau); // in ergs/s
    // printf("Tau is %f\n", tau);
    // printf("Flux is %f\n", flux);
    if (flux > 0){
        // find where to deposit
        double cos_the = cams[i].n_hat[2];
        double sin_the = sqrt(1 - cos_the * cos_the);
        double cos_phi = (sin_the == 0) ? 1 : cams[i].n_hat[0] / sin_the;
        double sin_phi = (sin_the == 0) ? 0 : cams[i].n_hat[1] / sin_the;
        double cos_psi = cos(cams[i].psi);
        double sin_psi = sin(cams[i].psi);
        double A11 = cos_the * cos_phi * cos_psi - sin_phi * sin_psi;
        double A12 = cos_the * sin_phi * cos_psi + cos_phi * sin_psi;
        double A13 = -sin_the * cos_psi;
        double A21 = -cos_the * cos_phi * sin_psi - sin_phi * cos_psi;
        double A22 = -cos_the * sin_phi * sin_psi + cos_phi * cos_psi;
        double A23 = sin_the * sin_psi;
        double x = A11 * p->pos[0] + A12 * p->pos[1] + A13 * p->pos[2];
        double y =
            A21 * p->pos[0] + A22 * p->pos[1] + A23 * p->pos[2]; // get coordinates

        int ik = (int)((x + cams[i].L) / cams[i].dx);
        int jk = (int)((y + cams[i].L) / cams[i].dx); // find pixel

        if ((ik >= 0) && (ik < cams[i].N) && (jk >= 0) && (jk < cams[i].N)) {
          cams[i].image[ik * cams[i].N + jk] += flux; // deposit flux

          CAMPHOTON ph;
          double nu0;
          ph.ik = ik;
          ph.jk = jk;
          // if (nu > pc.numid) {
          //   ph.lin = 'b';
          nu0 = pc.nub;
          // } else {
          //   ph.lin = 'a';
          //   nu0 = pc.nua;
          // }
          ph.flux = flux;
          ph.v = (int)(round((nu / nu0 - 1.0) * pc.c)); // cms^-1 = DOT(v_bulk, n_diff)
          // ph.T = p->T;
          fwrite(&ph, sizeof(CAMPHOTON), 1, fp); // write in spectrum map

          n_scatter += 1;
      }
    }

    /*
    printf("**********camera**********\n");
    printf("pos=%g %g %g\n", p->pos[0],p->pos[1],p->pos[2]);
    printf("nu=%g, emis=%g\n", nu-pc.nu0, p->emis);
    printf("x=%g, y=%g\n", x, y);
    printf("p_scatter=%g, tau=%g\n", p_scatter, tau);
    printf("flux=%g, i=%d, j=%d\n\n", flux, ik, jk);
    */
  }
}

double integrate_optical_depth(PHOTON *p) {
  int j;

  double tau = 0;
  while (p->fate == ACTIVE) {
    // gas thermal motion
    double T = CELL[p->cell_id].U->T;
    double vp = sqrt(2 * pc.kB * T / pc.mp);
    // double nuD = (vp / pc.c) * p->nu;
    // double a = pc.nuL / 2 / nuD;

    // gas bulk motion
    double v_bulk[3];
    for (j = 0; j < 3; j++) {
      v_bulk[j] = CELL[p->cell_id].U->vel[j] + All.HubbleParam * p->pos[j];
    }

    // doppler shift to fluid comoving frame
    double nu_com = p->nu - (p->nu / pc.c) * DOT(v_bulk, p->n_hat);
    // double x = (nu_com - p->nu) / nuD;

    // maximum step allowed
    double stepsize = maxstep_escape_cell(p->pos, p->n_hat, p->cell_id);

    // neutral hydrogen opacity
    // double sigma =
    //     pc.f * (pc.pi * pc.e * pc.e / pc.me / pc.c / nuD) * voigt_profile(x,
    //     a);
    // double gas_opac = sigma * CELL[p->cell_id].U->nh *
    //                   (All.HydrogenFraction * CELL[p->cell_id].U->rho /
    //                   pc.mp);

    // dust opacity
    double dust_opac =
        1.158e5 * CELL[p->cell_id].U->rho * CELL[p->cell_id].U->z;
    // printf("%f\n", dust_opac);

    // add to total optical depth
    tau += dust_opac * stepsize;

    // take the step
    for (j = 0; j < 3; j++)
      p->pos[j] += stepsize * p->n_hat[j];

    // check if escaped
    p->cell_id = find_cell_from_cell(p->pos, p->cell_id);
    if (p->cell_id == -1) {
      p->fate = ESCAPED;
      break;
    }
  }

  return tau;
}

void reduce_images() {
  // start time
  time_t start_tp, end_tp;
  time(&start_tp);

  if (ThisTask == 0) {
    printf("------------------------------------\n\n");
    printf("# Reduce images on %d cameras\n\n", All.NumberOfCameras);
  }

  int i, j, k;
  for (k = 0; k < All.NumberOfCameras; k++) {
    int N = cams[k].N;
    double *buffer = malloc(pow(N, 2) * sizeof(double));
    printf("\n%f\n", pow(N, 2));
    MPI_Allreduce(cams[k].image, buffer, pow(N, 2), MPI_DOUBLE, MPI_SUM,
                  MPI_COMM_WORLD);

    for (i = 0; i < N; i++) {
      for (j = 0; j < N; j++) {
        cams[k].image[i * N + j] = buffer[i * N + j];
      }
    }

    free(buffer);
  }

  time(&end_tp);
  float time_wasted = difftime(end_tp, start_tp) / 60;
  if (ThisTask == 0)
    printf("Time costed: %.2f minutes.\n\n", time_wasted);
}

void write_camera_output() {
  if (ThisTask == 0) {
    // create output file
    char fname[200];
    sprintf(fname, "%s/camera.h5", All.OutputDir);
    printf("------------------------------------\n\n");
    printf("# Writing cameras to file %s\n\n", fname);

    hid_t file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    herr_t status;

    // loop over cameras
    int i, j, k;
    for (i = 0; i < All.NumberOfCameras; i++) {
      char gname[10];
      sprintf(gname, "camera_%d", i);

      hid_t group_id = H5Gcreate1(file_id, gname, 0);

      // write attributes
      hid_t hdf5_dataspace = H5Screate(H5S_SCALAR);
      hid_t hdf5_attribute =
          H5Acreate2(group_id, "theta", H5T_NATIVE_DOUBLE, hdf5_dataspace,
                     H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &cams[i].theta);

      hdf5_dataspace = H5Screate(H5S_SCALAR);
      hdf5_attribute = H5Acreate2(group_id, "phi", H5T_NATIVE_DOUBLE,
                                  hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &cams[i].phi);

      hdf5_dataspace = H5Screate(H5S_SCALAR);
      hdf5_attribute = H5Acreate2(group_id, "L", H5T_NATIVE_DOUBLE,
                                  hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &cams[i].L);

      hdf5_dataspace = H5Screate(H5S_SCALAR);
      hdf5_attribute = H5Acreate2(group_id, "N", H5T_NATIVE_INT, hdf5_dataspace,
                                  H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &cams[i].N);

      hdf5_dataspace = H5Screate(H5S_SCALAR);
      hdf5_attribute = H5Acreate2(group_id, "dx", H5T_NATIVE_DOUBLE,
                                  hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &cams[i].dx);

      // write image
      hsize_t dims[2] = {cams[i].N, cams[i].N};
      status =
          H5LTmake_dataset_double(group_id, "image", 2, dims, cams[i].image);
    }

    // finish up
    for (i = 0; i < All.NumberOfCameras; i++) {
      free(cams[i].image);
    }
    free(cams);
    H5Fclose(file_id);
    printf("Done.\n\n");
  }

  printf("%ld scatter events in total\n", n_scatter);
  long ray_num;
  CAMPHOTON *ps;
  if (!(ps = malloc(n_scatter * sizeof(CAMPHOTON)))) {
    if (ThisTask == 0)
      printf("Failed to allocate memory (PHOTON).\n");
    endrun(4);
  }

  char infile[200];
  sprintf(infile, "%s/camphotons_%03d.dat", All.OutputDir, ThisTask);
  FILE *fp = fopen(infile, "rb");

  // fill in photons
  for (ray_num = 0; ray_num < n_scatter; ray_num++) {
    fread(&ps[ray_num], sizeof(CAMPHOTON), 1, fp);
  }
  fclose(fp);

  // write spectrum map to hdf5 file
  char outfile[200];
  sprintf(outfile, "%s/camphotons_%03d.h5", All.OutputDir, ThisTask);
  if (ThisTask == 0) {
    printf("------------------------------------\n\n");
    printf("# Writing spectrum maps to file %s\n\n", outfile);
  }

  hid_t file_id_spec =
      H5Fcreate(outfile, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  herr_t status_spec;

  // write datasets
  short *buf_short;
  char *buf_char;
  double *buf_double;
  int *buf_int;
  hsize_t dims1[1] = {n_scatter};

  buf_short = malloc(n_scatter * sizeof(short));
  for (ray_num = 0; ray_num < n_scatter; ray_num++)
    buf_short[ray_num] = ps[ray_num].ik;
  status_spec =
      H5LTmake_dataset_short(file_id_spec, "ipix", 1, dims1, buf_short);
  free(buf_short); // i pixel

  buf_short = malloc(n_scatter * sizeof(short));
  for (ray_num = 0; ray_num < n_scatter; ray_num++)
    buf_short[ray_num] = ps[ray_num].jk;
  status_spec =
      H5LTmake_dataset_short(file_id_spec, "jpix", 1, dims1, buf_short);
  free(buf_short); // j pixel

  // buf_char = malloc(n_scatter * sizeof(char));
  // for (ray_num = 0; ray_num < n_scatter; ray_num++)
  //   buf_char[ray_num] = ps[ray_num].lin;
  // status_spec = H5LTmake_dataset_char(file_id_spec, "lin", 1, dims1, buf_char);
  // free(buf_char); // H-alpha or beta

  buf_double = malloc(n_scatter * sizeof(double));
  for (ray_num = 0; ray_num < n_scatter; ray_num++)
    buf_double[ray_num] = ps[ray_num].flux;
  status_spec =
      H5LTmake_dataset_double(file_id_spec, "flux", 1, dims1, buf_double);
  free(buf_double); // flux

  buf_int = malloc(n_scatter * sizeof(int));
  for (ray_num = 0; ray_num < n_scatter; ray_num++)
    buf_int[ray_num] = ps[ray_num].v;
  status_spec = H5LTmake_dataset_int(file_id_spec, "v", 1, dims1, buf_int);
  free(buf_int); // Doppler velocity

  // buf_int = malloc(n_scatter * sizeof(int));
  // for (ray_num = 0; ray_num < n_scatter; ray_num++)
  //   buf_int[ray_num] = ps[ray_num].T;
  // status_spec = H5LTmake_dataset_int(file_id_spec, "T", 1, dims1, buf_int);
  // free(buf_int); // Temperature

  H5Fclose(file_id_spec);

  // finish up
  remove(infile);
  free(ps);
  if (ThisTask == 0)
    printf("Done.\n\n");
}
