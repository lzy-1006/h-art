#include <gsl/gsl_randist.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "allvar.h"
#include "proto.h"

void init_emission_from_cell() {
  long i;
  double T, nH, vol, alpha_a, alpha_b, prob_a;

  // allocate memory
  EMISSION_CELL.n = TOTAL_NUMBER_OF_LEAVES;
  // EMISSION_CELL.cdf_a = malloc(EMISSION_CELL.n * sizeof(double));
  // double *emissivity_a = malloc(EMISSION_CELL.n * sizeof(double));
  EMISSION_CELL.cdf_b = malloc(EMISSION_CELL.n * sizeof(double));
  double *emissivity_b = malloc(EMISSION_CELL.n * sizeof(double));

  // emissivity in each cell
  double total_emissivity_a = 0;
  double total_emissivity_b = 0;
  for (i = 0; i < EMISSION_CELL.n; i++) {
    T = CELL[LEAF_IDS[i]].U->T / 1.0e4; // in 10^4K
    nH = All.HydrogenFraction * CELL[LEAF_IDS[i]].U->rho / pc.mp;
    vol = pow(CELL[LEAF_IDS[i]].width, 3);

    // alpha_a =
    //     117 *
    //     pow(T, -0.942 - 0.031 * log(T)); // in 10^-15cm^3 s^-1, Draine Eq 14.8
    alpha_b =
        30.3 *
        pow(T, -0.874 - 0.058 * log(T)); // in 10^-15cm^3 s^-1, Draine Eq 14.9
    // P = 0.41 - 0.165*log10(T/1e4) - 0.015*pow(T/1e4,-0.44);
    // C = 2.2643351427e-6 * pow(T,-0.5) * exp(-118352.62246/T);

    double emiss = (1.0 - CELL[LEAF_IDS[i]].U->nh) * CELL[LEAF_IDS[i]].U->ne *
                   nH * nH * vol;
    // emissivity_a[i] = alpha_a * emiss; // s^-1
    // total_emissivity_a += emissivity_a[i];
    emissivity_b[i] = alpha_b * emiss; // s^-1
    total_emissivity_b += emissivity_b[i];
  }
  // printf("P(a) : P(b) = %f\n", total_emissivity_a / total_emissivity_b);
  // EMISSION_CELL.total_emissivity_a = total_emissivity_a * 1.0e-15;
  EMISSION_CELL.total_emissivity_b = total_emissivity_b * 1.0e-15;
  // EMISSION_CELL.prob_a =
  //     total_emissivity_a / (total_emissivity_a + total_emissivity_b);

  // cumulative distribution
  // EMISSION_CELL.cdf_a[0] = emissivity_a[0] / total_emissivity_a;
  EMISSION_CELL.cdf_b[0] = emissivity_b[0] / total_emissivity_b;
  for (i = 1; i < EMISSION_CELL.n; i++) {
    // EMISSION_CELL.cdf_a[i] =
    //     EMISSION_CELL.cdf_a[i - 1] + emissivity_a[i] / total_emissivity_a;
    EMISSION_CELL.cdf_b[i] =
        EMISSION_CELL.cdf_b[i - 1] + emissivity_b[i] / total_emissivity_b;
  }
  // free(emissivity_a);
  free(emissivity_b);
}

void emit_from_cell(PHOTON *p, FILE *fp) {
  int j;
  long i;
  double nu_com, r;
  // if ((r = gsl_rng_uniform(random_generator)) <=
  //     EMISSION_CELL.prob_a) { // H-alpha
  //   i = sample_cdf(EMISSION_CELL, EMISSION_CELL.cdf_a);
  //   nu_com = pc.nua; // sample frequency in comoving frame
  //   p->emis = EMISSION_CELL.total_emissivity_a /
  //             All.NphotonCell; // emissivity in s^-1
  // } else {                     // H-beta
    i = sample_cdf(EMISSION_CELL, EMISSION_CELL.cdf_b);
    nu_com = pc.nub; // sample frequency in comoving frame
    p->emis = EMISSION_CELL.total_emissivity_b /
              All.NphotonCell; // emissivity in s^-1
  // }

  // sample position in cell
  for (j = 0; j < 3; j++)
    p->pos[j] = CELL[LEAF_IDS[i]].min_x[j] +
                CELL[LEAF_IDS[i]].width * gsl_rng_uniform(random_generator);

  // // position is always origin, direction is always [1, 0, 0]
  // for (j=0; j<3; j++){
  //     p->pos[j] = 0.01;
  //     p->n_hat[j] = 0;
  // }
  // p->n_hat[0] = 1;

  // sample direction (nonrelativistic)
  sample_isotropic_direction(p->n_hat);

  p->cell_id = find_cell_from_cell(p->pos, LEAF_IDS[i]); // just to be sure

  // gas thermal motion
  double T = CELL[LEAF_IDS[i]].U->T / 1.0e4; // CGS units
  double vp_mode = sqrt(2.0 * pc.kB * T / pc.mp);
  float vp_sigma = sqrt(3.0 / 2.0 - 4.0 / pc.pi) * vp_mode;
  double vp = -1;
  while (vp < 0) {
    vp = gsl_ran_gaussian(random_generator, vp_sigma) + vp_mode;
  }
  double n_vp[3];
  sample_isotropic_direction(n_vp);

  // gas motion with thermal modification
  double v_bulk[3];
  for (j = 0; j < 3; j++) {
    v_bulk[j] =
        CELL[p->cell_id].U->vel[j] + All.HubbleParam * p->pos[j] + n_vp[j] * vp;
  }
  // p->T = (int)(T); // Trimmed, not rounded
  // printf("p->T %d\n", p->T);
  // doppler shift to lab frame
  p->nu = nu_com + (nu_com / pc.c) * DOT(v_bulk, p->n_hat);

  /*
  printf("i=%ld, cell_id=%ld (%ld)\n", i, LEAF_IDS[i], p->cell_id);
  printf("cell min_x=%g %g %g, width=%g\n",
  CELL[p->cell_id].min_x[0],CELL[p->cell_id].min_x[1],CELL[p->cell_id].min_x[2],CELL[p->cell_id].width);
  printf("photon pos=%g %g %g, nu=%g (%g)\n\n",
  p->pos[0],p->pos[1],p->pos[2],p->nu,nu_com);
  */

  p->fate = ACTIVE;

  // send to camera
  send_photon_to_camera(p, 0, fp);
}

// void emit_from_point_source(PHOTON* p)
// {
//     long source_id = sample_cdf(CDF_source);
//     p->source_id = source_id;
//     p->pos[0] = SOURCE[source_id].pos[0];
//     p->pos[1] = SOURCE[source_id].pos[1];
//     p->pos[2] = SOURCE[source_id].pos[2];
//     p->cell_id = SOURCE[source_id].cell_id;
//     SOURCE[source_id].Nemit++;
//
//     p->Q = SOURCE[source_id].Q/CDF_source.pdf[source_id]/All.n_photons_star;
//     //p->E =
//     E_emit_start*pow(10,log10(E_emit_stop/E_emit_start)*gsl_rng_uniform(random_generator));
//     p->E = pc.chi;
//
//     // isotropic emission
//     p->mu = -1+2.0*gsl_rng_uniform(random_generator);
//     p->phi = 2.0*pc.pi*gsl_rng_uniform(random_generator);
//     double sin_theta = sqrt(1-p->mu*p->mu);
//     p->n_hat[0] = sin_theta*cos(p->phi);
//     p->n_hat[1] = sin_theta*sin(p->phi);
//     p->n_hat[2] = p->mu;
// }

long sample_cdf(CDF p, double *pcdf) {
  double z = gsl_rng_uniform(random_generator);

  // mid, lower, and upper points
  long bm, bl = 0, bu = p.n - 1;

  // check if off the top or bottom
  if (z > pcdf[bu])
    return bu;
  if (z < pcdf[bl])
    return bl;

  // search
  while (bu - bl > 1) {
    bm = (bu + bl) / 2;
    if (z > pcdf[bm])
      bl = bm;
    else
      bu = bm;
  }

  return bu;
}
