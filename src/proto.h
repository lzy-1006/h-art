// from camera.c
void initialize_camera(void);
void send_photon_to_camera(PHOTON *, double, FILE *);
double integrate_optical_depth(PHOTON *);
void reduce_images(void);
void write_camera_output(void);

// from emission.c
void init_emission_from_cell(void);
void emit_from_cell(PHOTON *, FILE *);
void init_emission_from_point_source(void);
void emit_from_point_source(PHOTON *);
void emit_from_geometry(PHOTON *);
long sample_cdf(CDF, double *);

// from init.c
void read_parameter_file(char *);
void create_output_directory(void);
void fill_in_physical_constant(void);

// from main.c
void endrun(int);

// from mcrt.c
void run_mcrt(void);

// from photon.c
void write_photons(void);

// from transport.c
void transport(PHOTON *, FILE *);
void sample_isotropic_direction(double *);
void sample_mix_direction(double *, double);

// from tree.c
void read_octree_from_hdf5(char *);
long find_cell_from_cell(double *, long);
double maxstep_escape_cell(double *, double *, long);
void free_octree(void);

// from voigt.c
double cw(double);
double voigt_profile(double, double);
void sample_uatom(double, double, double, double *);
