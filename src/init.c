#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "allvar.h"
#include "proto.h"

void read_parameter_file(char *fname) {
  if (ThisTask == 0) {
    printf("------------------------------------\n\n");
    printf("# Reading parameters from file %s\n\n", fname);
  }

  // check if file exists
  FILE *fp;
  if (!(fp = fopen(fname, "r"))) {
    if (ThisTask == 0)
      printf("Fatal error: cannot open parameter file %s.\n", fname);
    endrun(2);
  }

  // adapted from GIZMO
  char buf[300], buf1[50], buf2[200], buf3[50];
  int i, j, nt, errorFlag = 0;
  int id[MAXTAGS];
  void *addr[MAXTAGS];
  char tag[MAXTAGS][50];

  nt = 0;
  strcpy(tag[nt], "OctreeFile");
  addr[nt] = &All.OctreeFile;
  id[nt++] = STRING;

  strcpy(tag[nt], "OutputDir");
  addr[nt] = &All.OutputDir;
  id[nt++] = STRING;

  strcpy(tag[nt], "HydrogenFraction");
  addr[nt] = &All.HydrogenFraction;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "NphotonCell");
  addr[nt] = &All.NphotonCell;
  id[nt++] = LONG;

  strcpy(tag[nt], "NphotonPointSource");
  addr[nt] = &All.NphotonPointSource;
  id[nt++] = LONG;

  strcpy(tag[nt], "NphotonGeometry");
  addr[nt] = &All.NphotonGeometry;
  id[nt++] = LONG;

  strcpy(tag[nt], "UnitMass_in_g");
  addr[nt] = &All.UnitMass_in_g;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "UnitLength_in_cm");
  addr[nt] = &All.UnitLength_in_cm;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "UnitVelocity_in_cm_per_s");
  addr[nt] = &All.UnitVelocity_in_cm_per_s;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "HubbleParam");
  addr[nt] = &All.HubbleParam;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "DustOpacity");
  addr[nt] = &All.DustOpacity;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "DustToMetalRatio");
  addr[nt] = &All.DustToMetalRatio;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "DustAlbedo");
  addr[nt] = &All.DustAlbedo;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "NumberOfCameras");
  addr[nt] = &All.NumberOfCameras;
  id[nt++] = INT;

  strcpy(tag[nt], "CameraInfoFile");
  addr[nt] = &All.CameraInfoFile;
  id[nt++] = STRING;

  // add more parameters here

  while (!feof(fp)) {
    fgets(buf, 300, fp);
    if (sscanf(buf, "%s%s%s", buf1, buf2, buf3) < 2)
      continue;
    if (buf1[0] == '#')
      continue;

    for (i = 0, j = -1; i < nt; i++) {
      if (strcmp(buf1, tag[i]) == 0) {
        j = i;
        tag[i][0] = 0;
        break;
      }
    }

    if (j >= 0) {
      switch (id[j]) {
      case INT:
        *((int *)addr[j]) = atoi(buf2);
        break;
      case LONG:
        *((long *)addr[j]) = atol(buf2);
        break;
      case DOUBLE:
        *((double *)addr[j]) = atof(buf2);
        break;
      case STRING:
        strcpy((char *)addr[j], buf2);
        break;
      }
      if (ThisTask == 0)
        printf("%-25s%s\n", buf1, buf2);
    } else {
      if (ThisTask == 0)
        printf("Tag '%s' ignored\n", buf1);
    }
  }
  fclose(fp);

  for (i = 0; i < nt; i++) {
    if (*tag[i]) {
      if (ThisTask == 0)
        printf("Error: tag '%s' is missing\n", tag[i]);
      errorFlag = 1;
    }
  }

  if (errorFlag == 1) {
    if (ThisTask == 0)
      printf("Error: missing parameter(s).\n");
    endrun(2);
  }

  if (ThisTask == 0)
    printf("\n");
}

void fill_in_physical_constant() {
  pc.pi = 3.14159265358979323846;
  pc.sqrt_pi = 1.7724538509055159;
  pc.sqrt2 = 1.4142135623730951;

  pc.c = 2.99792458e10;
  pc.h = 6.62607004e-27;
  pc.kB = 1.38064852e-16;

  pc.e = 4.80320427e-10;
  pc.mp = 1.67262158e-24;
  pc.me = 9.10938188e-28;

  pc.f = 0.4162;
  pc.nua = 4.566848e14;
  pc.nub = 6.166841e14;
  pc.numid = (pc.nua + pc.nub) / 2;
  pc.nuL = 9.936e7;
}
