#include <gsl/gsl_rng.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// MPI
int ThisTask, NTask;
// OpenMP
int maxThreads;

struct PhysicalConstant {
  double pi;      // pi
  double sqrt_pi; // sqrt(pi)
  double sqrt2;   // sqrt(2)

  double c;  // speed of light
  double h;  // Planck constant
  double kB; // Boltzmann constant

  double e;  // electron charge
  double mp; // proton mass
  double me; // electron mass

  double f;     // ocillator strength
  double nua;   // line-center frequency a
  double nub;   // line-center frequency b
  double numid; // mid-point btw nua & nub
  double nuL;   // natual broadening
} pc;

// gsl random number
gsl_rng *random_generator;

// parameter type
#define MAXTAGS 50
#define INT 0
#define LONG 1
#define DOUBLE 2
#define STRING 3

// model parameters
struct Parameters {
  char OctreeFile[200];
  char OutputDir[200];
  double HydrogenFraction;

  long NphotonCell;
  long NphotonPointSource;
  long NphotonGeometry;

  double UnitMass_in_g;
  double UnitLength_in_cm;
  double UnitVelocity_in_cm_per_s;
  double HubbleParam;

  double DustOpacity;
  double DustToMetalRatio;
  double DustAlbedo;

  int NumberOfCameras;
  char CameraInfoFile[200];

  // int n_mu;
  // int n_phi;
} All;

// octree
long TOTAL_NUMBER_OF_CELLS;
long TOTAL_NUMBER_OF_PARENTS;
long TOTAL_NUMBER_OF_LEAVES;
double MAX_DISTANCE_FROM0;

long *LEAF_IDS;
long *PARENT_IDS;

#define N_SUBCELL 2
#define N_SUBCELL_2 4
#define N_SUBCELL_3 8

typedef struct LOCALVALS_s {
  double vel[3]; /* velocity */
  double rho;    /* density */
  double nh;     /* H neutral fraction */
  double ne;     /* electron abundance per H atom */
  double z;      /* metallicity */
  double T;      /* Temperature */
} LOCALVAL;

struct CELL_STRUCT {
  double width;    /* width from one edge to another of the cell */
  double min_x[3]; /* x,y,z minima of the cell */

  long parent_ID;     /* ID number of parent cell for the given cell */
  int sub_cell_check; /* = 1 if there are sub-cells, 0 if not */
  long *sub_cell_IDs; /* ID numbers of the sub-cells contained (if present) */

  LOCALVAL *U; /* structure to hold local variables of the cell */
} * CELL;

// photon
typedef struct PHOTON_s {
  double pos[3]; /* current position */
  long cell_id;  /* cell id it resides */

  double n_hat[3]; /* propogation direction */
  double nu;       /* photon frequency in Hz */
  double emis;     /* emissivity in s^-1 */
  int T;           /* from emission cell */

  int fate;
} PHOTON;

typedef struct CAMPHOTON_s {
  short ik; // x pixel
  short jk; // y pixel
  double flux;
  int v; // Doppler velocity
  // char lin; // H-alpha or beta
  // int T; // Temperature of emission cell, used to separate phases of the gas
} CAMPHOTON;

#define ACTIVE 0  /* photon still propagates in domain */
#define KILLED -1 /* photon has been absorbed (by dust) */
#define ESCAPED 1 /* photon has escaped domain */

// emission
typedef struct CDF_s {
  long n;
  // double *cdf_a;
  // double total_emissivity_a;
  // double prob_a;
  double *cdf_b;
  double total_emissivity_b;
} CDF;

CDF EMISSION_CELL;

// camera
typedef struct CAMERA_s {
  double theta;
  double phi;
  double psi;
  double n_hat[3]; /* direction of camera */

  int N;         /* number of pixels in each dimension */
  double L;      /* half of side length */
  double dx;     /* pixel size */
  double *image; /* image array */
} CAMERA;

CAMERA *cams;
long n_scatter;

// general macros
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define DOT(a, b)                                                              \
  (((a)[0]) * ((b)[0]) + ((a)[1]) * ((b)[1]) + ((a)[2]) * ((b)[2]))
