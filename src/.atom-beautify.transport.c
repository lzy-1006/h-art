#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <omp.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>

#include "allvar.h"
#include "math.h"
#include "proto.h"

void transport(PHOTON *p, FILE *fp) {
  int i = 0, j;

  // while (i<10) {
  while (p->fate == ACTIVE) {
    // gas thermal motion (FAULTY)
    // double T = CELL[LEAF_IDS[i]].U->T / 1.0e4;
    // double vp_mode = sqrt(2.0 * pc.kB * T / pc.mp);
    // float vp_sigma = sqrt(3.0 / 2.0 - 4.0 / pc.pi) * vp_mode;
    // double vp = -1;
    // while (vp < 0) {
    //   vp = gsl_ran_gaussian(random_generator, vp_sigma) + vp_mode;
    // }
    // double nuD = (vp / pc.c) * p.nu;
    // double a = pc.nuL / 2 / nuD;

    // // gas bulk motion
    // double v_bulk[3];
    // for (j = 0; j < 3; j++)
    //   v_bulk[j] = CELL[p->cell_id].U->vel[j] + All.HubbleParam * p->pos[j];

    // // doppler shift to fluid comoving frame
    // double nu_com = p->nu - (p->nu / pc.c) * DOT(v_bulk, p->n_hat);
    // double x = (nu_com - p->nu) / nuD;

    // maximum step allowed
    double stepsize = maxstep_escape_cell(p->pos, p->n_hat, p->cell_id);
    double this_step = stepsize;
    int interaction = 0;

    // neutral hydrogen opacity
    // double sigma =
    //     pc.f * (pc.pi * pc.e * pc.e / pc.me / pc.c / nuD) * voigt_profile(x,
    //     a);
    // double gas_opac = sigma * CELL[p->cell_id].U->nh *
    //                   (All.HydrogenFraction * CELL[p->cell_id].U->rho /
    //                   pc.mp);
    // dust opacity
    double dust_opac = All.DustOpacity * All.DustToMetalRatio *
                       CELL[p->cell_id].U->rho * CELL[p->cell_id].U->z;
    double albedo = All.DustAlbedo;

    // random optical depth and step size
    double tot_opac = dust_opac;
    // double tau_gas = gas_opac * stepsize;
    double tau_tot = tot_opac * stepsize;
    // printf("Optical depth is %f\n", tau_tot);

    // find the next event
    double tau_r = -log(1 - gsl_rng_uniform(random_generator));
    if (tau_r < tau_tot) {
      this_step = tau_r / tot_opac;
      interaction = 1;
    }

    // take the step
    for (j = 0; j < 3; j++)
      p->pos[j] += this_step * p->n_hat[j];

    /*
    printf("**********photon**********\n");
    printf("i=%d, pos=%g %g %g\n",i,p->pos[0],p->pos[1],p->pos[2]);
    printf("n_hat=%g %g %g\n",p->n_hat[0],p->n_hat[1],p->n_hat[2]);
    printf("dnu=%g, dnu_com=%g\n",p->nu-pc.nu0, nu_com-pc.nu0);
    printf("x=%g, a=%g, nuD=%g, sigma=%g\n",x,a,nuD,sigma);
    printf("step=%g (%g) tau=%g (%g)\n\n",stepsize,this_step,tau_tot,tau_r);
    */

    // check if escaped
    p->cell_id = find_cell_from_cell(p->pos, p->cell_id);
    if (p->cell_id == -1) {
      p->fate = ESCAPED;
      break;
    }

    // do the interaction
    if (interaction) {
      // interact with dust
      double mu, phi, sin_theta;
      // if (gsl_rng_uniform(random_generator) < dust_opac / tot_opac) {
      if (gsl_rng_uniform(random_generator) < albedo) {
        // p->T = CELL[p->cell_id].U->T / 1.0e4; // CGS units
        sample_isotropic_direction(p->n_hat); // scatter
        send_photon_to_camera(p, 0, fp);
      } else {
        p->fate = KILLED; // absorption
        break;
      }
      //}
    }
    i++;
  }
}

void sample_isotropic_direction(double *n_out) {
  double mu = -1 + 2.0 * gsl_rng_uniform(random_generator);
  double phi = 2.0 * pc.pi * gsl_rng_uniform(random_generator);
  double sin_theta = sqrt(1 - mu * mu);
  n_out[0] = sin_theta * cos(phi);
  n_out[1] = sin_theta * sin(phi);
  n_out[2] = mu;
}

void sample_dipole_direction(double *n_out) {
  double mu;
  while (1) {
    double r1 = gsl_rng_uniform(random_generator);
    double r2 = gsl_rng_uniform(random_generator);
    mu = -1 + 2 * r1;
    if (r2 < (1 + mu * mu) / 2)
      break;
  }
  double phi = 2.0 * pc.pi * gsl_rng_uniform(random_generator);
  double sin_theta = sqrt(1 - mu * mu);
  n_out[0] = sin_theta * cos(phi);
  n_out[1] = sin_theta * sin(phi);
  n_out[2] = mu;
}

void sample_mix_direction(double *n_out, double p_dipole) {
  double r = gsl_rng_uniform(random_generator);
  if (r < p_dipole)
    sample_dipole_direction(n_out);
  else
    sample_isotropic_direction(n_out);
}
