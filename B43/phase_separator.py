import h5py
import numpy as np
import math

fname = 'B43H001-HL000-REF02Cold.hdf5'
lower = 0 #1e6
upper = 1e4

print(fname, lower, upper)
f = h5py.File(fname, 'r+')
Nleaf = f.attrs['Nleaf']
print(Nleaf)
n = Nleaf // math.log(Nleaf, 10)
T = f['T']
param = [f['nh'], f['rho'], f['z']]
vel = f['vel']

for i in range(Nleaf):
    if i % n == 0:
        print(i / Nleaf)
    t = T[i]
    if t < lower or t > upper:
        for p in param:
            p[i] = 0
        vel[i] = np.zeros((3,))
        T[i] = 0
print(fname, 'Modified')
f.close()
exit()
