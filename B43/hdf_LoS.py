import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
from mpl_toolkits.mplot3d import Axes3D
mplstyle.use('fast')

# fname = input('HDF5 file name: ')
fnames = ['photons_000.h5', 'photons_001.h5',
          'photons_002.h5', 'photons_003.h5']
skip = 1  # 50
nua = 4.566848  # in 10^14Hz
nub = 6.166841
o = (0, 0, -2e24)
# Direction of line of sight
theta = 0  # polar
phi = 0  # azimuthal
r = 6.37e18  # Radius of line of sight
half_size = 52 * 3.09e21  # for the box


class Line:
    def __init__(self, ori, vec):
        self.ori = ori
        self.vec = vec


def distance_from_pnt_to_line(p, l):
    a = l.ori
    n = l.vec
    a_p = np.subtract(a, p)  # a_hat - p_hat
    vec = np.subtract(a_p, np.multiply(np.dot(a_p, n), n))
    return np.sqrt(vec.dot(vec))


print('Max theta can be', np.arctan(
    (half_size - o[0]) / np.cos(phi) / (- half_size - o[2])))

fat = []
nha = []
fskip = []
pos = []
for fname in fnames:
    print('Reading file', fname)
    f = h5py.File(fname, 'r')
    fat.extend(f['fate'][::skip])
    nha.extend(f['n_hat'][::skip])
    fskip.extend(f['nu'][::skip])
    pos.extend(f['pos'][::skip])
    f.close()

sin = np.sin(theta)
sight = (sin * np.cos(phi), sin * np.sin(phi), np.cos(theta))
if np.dot(o, sight) > 0:
    print('Line of sight does not cross domain')
    theta = -theta
    phi = -phi
    sight = np.multiply(-1, sight)
    print('Line of sight reversed')
print('Looking in direction', sight)
los = Line(o, sight)
pho = [{'n_hat': nha[i], 'nu': fskip[i], 'pos': pos[i]}
       for i in range(len(pos)) if fat[i] == 1]
print(pho[0])
print(len(pho), 'photons')
pholos = []
while len(pholos) < 1 and r < half_size:
    print('Radius of line of sight is', r)
    for p in pho:
        posi = p['pos']
        nhat = p['n_hat']
        if distance_from_pnt_to_line(posi, los) < r and distance_from_pnt_to_line(o, Line(posi, nhat)) <= r and np.dot(nhat, sight) < 0:
            pholos.append(p)
    r = r * 2
if len(pholos) == 0:
    print('Nothing in sight')
    exit()
print(len(pholos), 'photons visible')
# nha = [p['n_hat'] for p in pholos]
f = [p['nu'] for p in pholos]
fskip = f
pos = [p['pos'] for p in pholos]

# Coordinates in 3D
coor = [[j[i] for j in pos]
        for i in range(len(pos[0]))]  # i is spatial dimension
# dire = [[nha[j][i] for j in range(len(nha))]
#         for i in range(len(nha[0]))]
print('Coordinate parsing complete')

numid = (nua + nub) / 2
bet = 0
v = []  # Doppler velocities in c
for nu in fskip:
    nu = nu / 1.0e14
    nu0 = nua
    if nu > numid:
        bet += 1
        nu0 = nub
    v.append(abs(nu / nu0 - 1))
# vskip = v[::skip]
print(bet * 100 / len(fskip), '% are H-beta')
print('Average Doppler velocity is', np.mean(v))
print('Highest Doppler velocities are', sorted(v)[:: -1][: 3])

fig = plt.figure()

dist = fig.add_subplot(221, projection='3d')
dist.set_xlim(-half_size, half_size)
dist.set_ylim(-half_size, half_size)
dist.set_zlim(-half_size, half_size)
dist.scatter(coor[0], coor[1], coor[2], c=fskip, cmap='bwr',
             alpha=0.5, s=[4e4 * u for u in v])
# dist.hist(coor[0], alpha=0.5, bins=256)
# dist.quiver(coor[0], coor[1], coor[2], dire[0],
#             dire[1], dire[2], fskip, cmap='bwr', length=1e-4, alpha=0.5)
dist.set_title('Spatial photon distribution')

spec = fig.add_subplot(222)
spec.hist(fskip)
spec.set_title('Spectrum')

xs = sorted(coor[0])
xdat = []
for i in range(len(coor[0])):
    xdat.append([coor[0][i], v[i]])
xds = sorted(xdat, key=lambda i: i[0])
xv = [x[1] for x in xds]
distx = fig.add_subplot(223)
distx.plot(xs, xv, alpha=0.3, label='Velocities along x')
distx.hist2d(xs, xv)
distx.set_title('Velocities along x')
# xplo = distx.hist(coor[0], label='spatial distribution')
# xf = [x[1] for x in xds]
# xfplo = distx.plot(xs, xf, label='frequency along x')

spec = fig.add_subplot(224)
spec.hist(v)
spec.set_title('Doppler velocities')

plt.show()
