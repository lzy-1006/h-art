import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
from mpl_toolkits.mplot3d import Axes3D
mplstyle.use('fast')

# fname = input('HDF5 file name: ')
fname = 'photons_000.h5'
skip = 100  # 50
nua = 4.566848  # in 10^14Hz
nub = 6.166841
numid = (nua + nub) / 2

f = h5py.File(fname, 'r')
print('Keys are ', f.keys())
print(f.items())

fat = list(f['fate'][:])
print(fat.count(-1), 'absorbed')
print(fat.count(1), 'escaped')

freq = list(f['nu'][:])
bet = 0
v = []  # Doppler velocities in c
fskip = freq[::skip]
for nu in freq:
    nu = nu / 1.0e14
    nu0 = nua
    if nu > numid:
        bet += 1
        nu0 = nub
    v.append(abs(nu / nu0 - 1))
vskip = v[::skip]
print(sorted(v)[::-1][:5])
v_bar = np.mean(vskip)
print('Average Doppler velocity is', v_bar)
print(bet * 100 / len(freq), '% are H-beta')

pos = f['pos'][::skip]
# Coordinates in 3D
coor = [[j[i] for j in pos]
        for i in range(pos.shape[1])]  # i is spatial dimension
# nha = f['n_hat'][::skip]
# dire = [[vskip[j] * nha[j][i] for j in range(len(nha))]
#         for i in range(nha.shape[1])]
print('Coordinate parsing complete')

f.close()

fig = plt.figure()

dist = fig.add_subplot(221, projection='3d')
dist.scatter(coor[0], coor[1], coor[2], c=fskip, cmap='bwr',
             alpha=0.5, s=[20 * u / v_bar for u in vskip])  # 4e4
#dist.hist(coor[0], alpha=0.5, bins=256)
# dist.quiver(coor[0], coor[1], coor[2], dire[0],
#             dire[1], dire[2], fskip, cmap='bwr', length=1, alpha=0.5)
dist.set_title('Spatial photon distribution')

spec = fig.add_subplot(222)
spec.hist(freq)
spec.set_title('Spectrum')

xs = sorted(coor[0])
xdat = []
for i in range(len(coor[0])):
    xdat.append([coor[0][i], vskip[i]])
xds = sorted(xdat, key=lambda i: i[0])
xv = [x[1] for x in xds]
distx = fig.add_subplot(223)
distx.plot(xs, xv, alpha=0.3, label='Velocities along x')
distx.hist2d(xs, xv, bins=100)
distx.set_title('Velocities along x')
# xplo = distx.hist(coor[0], label='spatial distribution')
# xf = [x[1] for x in xds]
# xfplo = distx.plot(xs, xf, label='frequency along x')

spec = fig.add_subplot(224)
spec.hist(v, bins=500)
spec.set_title('Doppler velocities')

plt.show()
