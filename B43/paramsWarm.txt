# parameter file

OctreeFile					/Users/ast/h-art/B43/B43H001-HL000-REF02Warm.hdf5
OutputDir					/Users/ast/h-art/B43/Warm
HydrogenFraction			1

NphotonCell				1000
NphotonPointSource			0
NphotonGeometry				0

UnitMass_in_g				1.989e43
UnitLength_in_cm			3.085678e21
UnitVelocity_in_cm_per_s	1e5
HubbleParam                 0

DustOpacity					2.3e4
DustToMetalRatio			0.4
DustAlbedo					0.65

NumberOfCameras				1
CameraInfoFile				h-art/src/camera.txt

