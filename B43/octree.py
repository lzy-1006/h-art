import numpy as np
import ctypes
import os
import h5py
import matplotlib.pyplot as plt
import matplotlib.colors as colors
# import matplotlib.style as mplstyle
# from mpl_toolkits.mplot3d import Axes3D
# mplstyle.use('fast')

HydrogenFraction = 1
mp = 1.67262158e-24  # mass of proton
Npix = 256
los = np.array((0, 0, 1))

# build octree from particle data

def build_octree_from_particle(pos, vel, m, h, nh, ne, u, z, cen=0, MAX_DISTANCE_FROM0=0, TREE_MIN_SIZE=0.01, TREE_MAX_NPART=32, fname="test.hdf5"):

    # if center is not given, calculate from points
    cen = np.array(cen, ndmin=1, dtype="float64")
    if ((len(cen) != 3) | (MAX_DISTANCE_FROM0 <= 0)):
        Lx = (np.max(pos[:, 0]) - np.min(pos[:, 0])) / 2
        Ly = (np.max(pos[:, 1]) - np.min(pos[:, 1])) / 2
        Lz = (np.max(pos[:, 2]) - np.min(pos[:, 2])) / 2
        MAX_DISTANCE_FROM0 = 1.001 * max(Lx, Ly, Lz)

        xc = (np.max(pos[:, 0]) + np.min(pos[:, 0])) / 2
        yc = (np.max(pos[:, 1]) + np.min(pos[:, 1])) / 2
        zc = (np.max(pos[:, 2]) + np.min(pos[:, 2])) / 2
    else:
        xc, yc, zc = cen[0], cen[1], cen[2]
    print("Making octree from particle ...")
    print(4 * " " + "centering at", cen)

    # to be safe, trim particles
    pos[:, 0] -= xc
    pos[:, 1] -= yc
    pos[:, 2] -= zc
    ok = (pos[:, 0] > -MAX_DISTANCE_FROM0) & (pos[:, 0] < MAX_DISTANCE_FROM0) & (pos[:, 1] > -MAX_DISTANCE_FROM0) & (
        pos[:, 1] < MAX_DISTANCE_FROM0) & (pos[:, 2] > -MAX_DISTANCE_FROM0) & (pos[:, 2] < MAX_DISTANCE_FROM0)
    pos, vel, m, h = pos[ok], vel[ok], m[ok], h[ok]
    nh, ne, u, z = nh[ok], ne[ok], u[ok], z[ok]

    # check how many particles in the domain
    N_gas = len(m)
    N_gas = np.int32(N_gas)
    if (N_gas < 1):
        print("Error: no particle in the domain.")
        return

    # fill in data structure: note the order
    data = np.zeros((N_gas, 12), dtype="float32")
    data[:, 0:3], data[:, 3:6], data[:, 6], data[:, 7] = pos, vel, m, h
    data[:, 8], data[:, 9], data[:, 10], data[:, 11] = nh, ne, u, z

    # convert data type
    data_ctypes = data.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
    cen_ctypes = cen.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    # call the function
    exec_call = "/Users/ast/octreert/octree/src/octree.so"
    routine = ctypes.cdll[exec_call]

    routine.build_octree_from_particle(ctypes.c_int(N_gas), data_ctypes, cen_ctypes, fname.encode(
        "ascii"), ctypes.c_double(MAX_DISTANCE_FROM0), ctypes.c_double(TREE_MIN_SIZE), ctypes.c_int(TREE_MAX_NPART))

    return


# compute column density from position using octree
def compute_column_density(pos, theta, phi, rmax=0, fname="test.hdf5"):

    # read tree header from file
    f = h5py.File(fname, "r")
    MAX_DISTANCE_FROM0 = f.attrs["Boxsize"]
    TOTAL_NUMBER_OF_LEAVES = f.attrs["Nleaf"]
    f.close()
    print("Calculating column density from octree %s" % fname)
    print(4 * " " + "octree half side-length", MAX_DISTANCE_FROM0)

    # check how many particles are in the domain
    ok = (pos[:, 0] > -MAX_DISTANCE_FROM0) & (pos[:, 0] < MAX_DISTANCE_FROM0) & \
        (pos[:, 1] > -MAX_DISTANCE_FROM0) & (pos[:, 1] < MAX_DISTANCE_FROM0) & \
        (pos[:, 2] > -MAX_DISTANCE_FROM0) & (pos[:, 2] < MAX_DISTANCE_FROM0)
    print(4 * " " + "total %d sources given" % len(pos))
    if (len(pos[ok]) < len(pos)):
        print(4 * " " + "warning: only %d sources in the domain." %
              len(pos[ok]))

    # convert data format
    pos = np.array(pos, ndmin=2, dtype="float64")
    theta = np.array(theta, ndmin=1, dtype="float64")
    phi = np.array(phi, ndmin=1, dtype="float64")
    N_star, N_los = pos.shape[0], theta.shape[0]
    pos_ctypes = pos.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    theta_ctypes = theta.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    phi_ctypes = phi.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    rmax_ctypes = ctypes.c_double(np.float64(rmax))
    N_star_ctypes = ctypes.c_int(np.int32(N_star))
    N_los_ctypes = ctypes.c_int(np.int32(N_los))

    # allocate returned arrays
    nh_out_cast = (ctypes.c_double * N_los) * N_star  # two-dimensional array
    los_NH_out = nh_out_cast()  # hydrogen column density
    los_NH_nh_out = nh_out_cast()  # neutral H column density
    los_NH_z_out = nh_out_cast()  # metal column denisty

    # call the function
    exec_call = "/Users/ast/octreert/octree/src/octree.so"

    routine = ctypes.cdll[exec_call]
    routine.compute_column_density(N_star_ctypes, pos_ctypes, N_los_ctypes, theta_ctypes,
                                   phi_ctypes, rmax_ctypes, fname.encode("ascii"), los_NH_out, los_NH_nh_out, los_NH_z_out)

    los_NH = np.ctypeslib.as_array(los_NH_out)
    los_NH_nh = np.ctypeslib.as_array(los_NH_nh_out)
    los_NH_z = np.ctypeslib.as_array(los_NH_z_out)

    return los_NH, los_NH_nh, los_NH_z


# get projected image from octree
def project_tree_on_image(fname="test.dat", **kwargs):

    # assign keywords
    cen = kwargs["cen"] if "cen" in kwargs else [0, 0, 0]
    L = kwargs["L"] if "L" in kwargs else 5
    Lz = kwargs["Lz"] if "Lz" in kwargs else L
    Nx = kwargs["Nx"] if "Nx" in kwargs else 100
    theta = kwargs["theta"] if "theta" in kwargs else 0
    phi = kwargs["phi"] if "phi" in kwargs else 0
    psi = kwargs["psi"] if "psi" in kwargs else 0

    print("Making projected gas image from octree %s" % fname)
    print(4 * " " + "image centered on %.3g %.3g %.3g" %
          (cen[0], cen[1], cen[2]))
    print(4 * " " + "image size L=%.3g, Lz=%.3g, N=%d pixels on each side" % (L, Lz, Nx))
    print(4 * " " + "coordinate system theta=%.2f, phi=%.2f, psi=%.2f" %
          (theta, phi, psi))

    # convert data types
    L_ctypes = ctypes.c_double(np.float64(L))
    Lz_ctypes = ctypes.c_double(np.float64(Lz))
    Nx_ctypes = ctypes.c_int(np.int32(Nx))
    theta_ctypes = ctypes.c_double(np.float64(theta))
    phi_ctypes = ctypes.c_double(np.float64(phi))
    psi_ctypes = ctypes.c_double(np.float64(psi))
    cen = np.array(cen, ndmin=1, dtype=np.float64)
    cen_ctypes = cen.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    # allocate returned arrays
    data_out_cast = (ctypes.c_double * Nx) * Nx
    data_out = data_out_cast()
    data_nh_out = data_out_cast()
    data_z_out = data_out_cast()

    # call the function
    exec_call = "/Users/ast/octreert/octree/src/octree.so"
    routine = ctypes.cdll[exec_call]
    routine.project_tree_on_image(cen_ctypes, L_ctypes, Lz_ctypes, Nx_ctypes, theta_ctypes,
                                  phi_ctypes, psi_ctypes, fname.encode("ascii"), data_out, data_nh_out, data_z_out)
    data = np.ctypeslib.as_array(data_out)
    data_nh = np.ctypeslib.as_array(data_nh_out)
    data_z = np.ctypeslib.as_array(data_z_out)

    return data, data_nh, data_z

def trim(n):
    if n >= Npix:
        return Npix - 1
    elif n < 0:
        return 0
    else:
        return n

def default_emptiness(defa, img):
    for x in img:
        for y in x:
            if not y:
                y.append(defa)
    return img

# octree structure
class TREE:

    def __init__(self, fname):

        self.k = 0 if os.path.isfile(fname) else -1
        self.fname = fname

        return

    def load(self):

        if (self.k == -1):
            print("File does not exist.")
            return self
        print("Loading octree from file %s..." % self.fname)

        f = h5py.File(self.fname, "r")
        self.TOTAL_NUMBER_OF_CELLS = f.attrs["Ncell"]
        self.TOTAL_NUMBER_OF_PARENTS = f.attrs["Nparent"]
        self.TOTAL_NUMBER_OF_LEAVES = f.attrs["Nleaf"]
        self.MAX_DISTANCE_FROM0 = f.attrs["Boxsize"]
        # self.TREE_MIN_SIZE = f.attrs["MinSize"]
        # self.TREE_MAX_NPART = f.attrs["MaxNpart"]
        # self.CENTER = f.attrs["Center"]

        self.width = f["width"][...]
        self.min_x = f["min_x"][...]  # cell corner
        self.p = np.copy(self.min_x)  # cell center
        for k in range(3):
            self.p[:, k] += self.width / 2
        self.parent_ID = f["parent_ID"][...]
        self.sub_cell_check = f["sub_cell_check"][...]
        self.sub_cell_IDs = f["sub_cell_IDs"][...]  # parent only
        self.vel = f["vel"][...]  # children only (all below)
        self.rho = f["rho"][...]
        self.nh = f["nh"][...]
        self.ne = self.nh
        # self.ne = f["ne"][...]
        self.z = f["z"][...]
        self.T = f["T"][...]
        f.close()

        self.vol = self.width ** 3
        self.nHy = np.multiply(self.rho, (HydrogenFraction / mp))
        self.emiss = [(1 - self.nh[i]) * self.ne[i] *
                      (self.nHy[i] ** 2) * self.vol[i] for i in range(len(self.nh))]
        # in 10^-15cm^3 s^-1, Draine Eq 14.8
        self.alpha_a = [
            117 * T ** (-0.942 - 0.031 * np.log(T)) for T in np.divide(self.T, 1e4)]
        self.emis_a = np.multiply(self.alpha_a, self.emiss)
        # in 10^-15cm^3 s^-1, Draine Eq 14.9
        self.alpha_b = [
            30.3 * T ** (-0.874 - 0.058 * np.log(T)) for T in np.divide(self.T, 1e4)]
        self.emis_b = np.multiply(self.alpha_b, self.emiss)

        # set up pointer from cell to quantities
        self.pointer = np.zeros(self.TOTAL_NUMBER_OF_CELLS, dtype=int)
        self.pointer[self.sub_cell_check == 1] = np.arange(
            self.TOTAL_NUMBER_OF_PARENTS)
        self.pointer[self.sub_cell_check == 0] = np.arange(
            self.TOTAL_NUMBER_OF_LEAVES)

        print("Octree loaded")
        return self

    def project_tree_on_image(self, **kwargs):

        if "cen" not in kwargs:
            kwargs["cen"] = [0, 0, 0]
        if "L" not in kwargs:
            kwargs["L"] = 5
        if "Lz" not in kwargs:
            kwargs["Lz"] = kwargs["L"]
        if "Nx" not in kwargs:
            kwargs["Nx"] = 100
        if "theta" not in kwargs:
            kwargs["theta"] = 0
        if "phi" not in kwargs:
            kwargs["phi"] = 0
        if "psi" not in kwargs:
            kwargs["psi"] = 0

        return project_tree_on_image(self.fname, **kwargs)

    def is_pos_in_cell(self, pos, cell_id):

        in_cell_check = 1
        for k in range(3):
            if (pos[k] < self.min_x[cell_id, k]):
                in_cell_check = 0
            if (pos[k] >= self.min_x[cell_id, k] + self.width[cell_id]):
                in_cell_check = 0

        return in_cell_check

    def find_cell_from_cell(self, pos, cell_id):

        # check if outside domain
        for k in range(3):
            if (pos[k] < -self.MAX_DISTANCE_FROM0):
                return -1
            if (pos[k] >= self.MAX_DISTANCE_FROM0):
                return -1

        # invalid cell index
        if ((cell_id < 0) | (cell_id >= self.TOTAL_NUMBER_OF_CELLS)):
            cell_id = 0

        # find parent cell it belongs
        in_cell_check = self.is_pos_in_cell(pos, cell_id)
        while (in_cell_check == 0):
            cell_id = self.parent_ID[cell_id]
            if (cell_id == -1):
                return -1
            in_cell_check = self.is_pos_in_cell(pos, cell_id)

        # find the finest-level cell
        while (self.sub_cell_check[cell_id] == 1):
            nx = np.int((pos[0] - self.min_x[cell_id, 0]) /
                        (self.width[cell_id] / 2))
            ny = np.int((pos[1] - self.min_x[cell_id, 1]) /
                        (self.width[cell_id] / 2))
            nz = np.int((pos[2] - self.min_x[cell_id, 2]) /
                        (self.width[cell_id] / 2))
            pointer_id = self.pointer[cell_id]
            cell_id = self.sub_cell_IDs[pointer_id, 4 * nx + 2 * ny + nz]

        return cell_id


t = TREE('B43H001-HL000-REF02.hdf5').load()

# sub_cell_check = np.array(t.sub_cell_check)
# p = []
# for i in range(len(t.p)):
#     if i % 1000000 == 0:
#         print(i)
#     if sub_cell_check[i] == 0:
#         p.append(t.p[i])
# print('p has length', len(p))
# np.savetxt('cells.csv', p, delimiter=',')

# p = np.loadtxt('cells.csv', delimiter = ',')
# print('Cells loaded')
#
# dx = t.MAX_DISTANCE_FROM0 / (Npix / 2)
# im_rho = [[[] for i in range(Npix)] for j in range(Npix)]
# im_emisa = [[[] for i in range(Npix)] for j in range(Npix)]
# im_emisb = [[[] for i in range(Npix)] for j in range(Npix)]
# im_v = [[[] for i in range(Npix)] for j in range(Npix)]
# im_T = [[[] for i in range(Npix)] for j in range(Npix)]
#
# im_ratio = 0.5 * np.ones((Npix, Npix))
#
# for i in range(len(p)):
#     pos = p[i]
#     vel = t.vel[i]
#     x = int(round(pos[0] / dx + Npix // 2))
#     y = int(round(pos[1] / dx + Npix // 2))
#     x = trim(x)
#     y = trim(y)
#     im_rho[x][y].append(t.rho[i])
#     im_emisa[x][y].append(t.emis_a[i])
#     im_emisb[x][y].append(t.emis_b[i])
#     im_v[x][y].append(vel.dot(los))
#     im_T[x][y].append(t.T[i])
# print('Octree flattened')
#
# default_emptiness(0, im_rho)
# default_emptiness(0, im_emisa)
# default_emptiness(0, im_emisb)
# default_emptiness(0, im_v)
# default_emptiness(0, im_T)
# im_rho = np.array([[np.mean(y) for y in x] for x in im_rho])
# im_emisa = np.array([[np.mean(y) for y in x] for x in im_emisa])
# im_emisb = np.array([[np.mean(y) for y in x] for x in im_emisb])
# im_v = np.array([[np.mean(y) for y in x] for x in im_v])
# im_T = np.array([[np.mean(y) for y in x] for x in im_T])
# print('Calculating line ratios')
# im_sum = np.add(im_emisa, im_emisb)
# for x in range(Npix):
#     for y in range(Npix):
#         su = im_sum[x][y]
#         if su:
#             im_ratio[x][y] = im_emisa[x][y] / su
# np.save('img_rho', im_rho)
# np.save('img_emisa', im_emisa)
# np.save('img_emisb', im_emisb)
# np.save('img_v', im_v)
# np.save('img_ratio', im_ratio)
# np.save('img_T', im_T)

# im_rho = np.load('img_rho.npy', allow_pickle=True)
# im_emisa = np.load('img_emisa.npy', allow_pickle=True)
# im_emisb = np.load('img_emisb.npy', allow_pickle=True)
# im_v = np.load('img_v.npy', allow_pickle=True)
# im_ratio = np.load('img_ratio.npy', allow_pickle=True)
# im_T = np.load('img_T.npy', allow_pickle=True)
#
# plt.style.use('dark_background')
# fig, axs = plt.subplots(ncols = 3, nrows = 2)
# axs = np.reshape(axs, 6)
# imgs = (im_rho, im_emisa, im_emisb, im_v, im_ratio, im_T)
# tits = ('Density', 'H-a Emissivity', 'H-b Emissivity', 'Velocity', 'H-a Ratio', 'Temperature')
# ims = [[] for i in imgs]
# for i in range(len(imgs)):
#     if i == 3: #velocity
#         peak = np.ptp(imgs[i])
#         ims[i] = axs[i].imshow(imgs[i], cmap='bwr', interpolation='gaussian', vmin=-peak, vmax=peak)
#     elif i in range(3):
#         maxi = np.max(imgs[i])
#         ims[i] = axs[i].imshow(imgs[i], cmap='magma', interpolation='gaussian', norm=colors.LogNorm(vmin = maxi / 1e8))
#     else:
#         ims[i] = axs[i].imshow(imgs[i], cmap='magma', interpolation='gaussian', norm=colors.LogNorm())
#     # axs[i].contour(imgs[i], alpha=0.1)
#     axs[i].set_title(tits[i])
#     cbar = fig.colorbar(ims[i], ax = axs[i])
#     cbar.minorticks_on()

data, data_nh, data_z = t.project_tree_on_image(Nx=272, L=5, cen=(0, 0, -52))
dat = (data, data_nh, data_z)
var = ('NH', 'nh', 'z')
fig, axs = plt.subplots(ncols = 3)
ims = [[] for i in dat]
for l in range(len(dat)):
    maxi = np.max(dat[l])
    ims[l] = axs[l].imshow(dat[l], cmap='magma', interpolation='gaussian', norm=colors.LogNorm())
    cbar = fig.colorbar(ims[l], ax = axs[l])
    cbar.minorticks_on()
    axs[l].set_title(var[l])
plt.show()
