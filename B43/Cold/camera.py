import h5py
import numpy as np
import statistics
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as mticker

# fname = input('HDF5 file name: ')
fname = 'camera.h5'
fnames = ['camphotons_000.h5', 'camphotons_001.h5', 'camphotons_002.h5', 'camphotons_003.h5']
Npix = 256
cold = 1 # in 10^4 K
hot = 100
v_max = 60 #1300
# nua = 4.566848  # in 10^14Hz
# nub = 6.166841
# numid = (nua + nub) / 2
# c = 2.9979e5

def default_emptiness(defa, img):
    for y in img:
        for z in y:
            if z == []:
                z.append(defa)
    return img

def clip8(dat):
    mini = np.min(dat)
    maxi = np.max(dat)
    if mini == 0 or mini != 0 and maxi / mini > 1e8:
        mini = maxi / 1e8
    return mini, maxi

def averie(dat, weights=None):
    if len(dat) == 0:
        return 0
    elif len(dat) == 1:
        return dat[0]
    else:
        if weights:
            return sum(x * y for x, y in zip(dat, weights)) / sum(weights)
        else:
            return statistics.mean(dat)

def draw(ax, dat, type='log', alpha=1, cmap='magma'):
    if type == 'v':
        peak = np.ptp(dat)
        if peak > v_max:
            peak = v_max
        imag = ax.imshow(dat, alpha=alpha, cmap='bwr', norm=colors.Normalize(vmin=-peak, vmax=peak))
        fmt = mticker.FormatStrFormatter('%s km/s')
    elif type == 'ratio':
        imag = ax.imshow(dat, alpha=alpha, cmap='bwr',  interpolation='nearest', norm=colors.LogNorm(vmin=0.25, vmax=1))
        fmt = mticker.FormatStrFormatter('%s')
    else:
        mini, maxi = clip8(dat)
        # if mini: # and dat is not map_T and dat is not im_T:
        #     imag = ax.imshow(dat, alpha=alpha,cmap=cmap,  interpolation='nearest', norm=colors.LogNorm(vmin=mini))
        # else:
        imag = ax.imshow(dat, alpha=alpha, cmap=cmap, interpolation='nearest', norm=colors.LogNorm())
        fmt = mticker.FormatStrFormatter('%s K')
    cbar = fig.colorbar(imag, ax=ax, format=fmt)
    cbar.minorticks_on()
    return imag

# ims = np.load('ims.npy', allow_pickle=True)
map_v_mean = np.load('map_v_mean.npy', allow_pickle=True) / 1e5
# map_T = np.load('map_T.npy', allow_pickle=True)
# spectral_ratio = np.load('spectral_ratio.npy', allow_pickle=True)
# im_T = np.load('img_T.npy', allow_pickle=True) # from octree

# ipix = []
# jpix = []
# flux = []
# v = []
# # lin = []
# # T = []
# for fn in fnames:
#     fmap = h5py.File(fn, 'r')
#     print(fmap.keys())
#     ipix = np.concatenate((ipix, np.array(fmap['ipix']))).astype(int)
#     jpix = np.concatenate((jpix, np.array(fmap['jpix']))).astype(int)
#     flux = np.concatenate((flux, np.array(fmap['flux'])))
#     v = np.concatenate((v, np.array(fmap['v'])))
#     # T = np.concatenate((T, np.array(fmap['T']))) # in 10^4K
#     # lin = np.concatenate((lin, np.array(fmap['lin'])))
#     fmap.close()
# print('File reading complete')
#
# # map_T = [[[] for i in range(Npix)] for j in range(Npix)]
# # [Cold: [a, b], warm: [,], hot: [,]]
# # ims = np.zeros((Npix, Npix)) for j in range(2)] for i in range(3)]
# map_flux = [[[] for i in range(Npix)] for j in range(Npix)] # for ln in range(2)] for phas in range(3)]
# map_v = [[[] for i in range(Npix)] for j in range(Npix)] # for ln in range(2)] for phas in range(3)]
#
# for i in range(len(ipix)):
#     ip = ipix[i]
#     jp = jpix[i]
#     flu = flux[i]
#     vel = v[i]
#     # line = lin[i]
#     # temp = T[i]
#     # phas = 1 # Warm by default
#     # if temp < cold:
#     #     phas = 0
#     # elif temp >= hot:
#     #     phas = 2
#     # linear = -1
#     # if line == 97:  # 'a' in ASCII
#     #     linear = 0
#     # elif line == 98: # 'b'
#     #     linear = 1
#     # else:
#     #     print('Unknown frequency ' * 5)
#     #     exit()
#     # ims[ip][jp] += flu
#     # if temp != 0:
#     #     map_T[ip][jp].append(temp)
#     map_flux[ip][jp].append(flu)
#     map_v[ip][jp].append(vel)
# print('Photons deposited')
#
# # np.save('ims', ims)
# # print('Imgs saved')
# #
# # # default_emptiness(0, map_T)
# # # map_T = [[averie(map_T[i][j]) for j in range(Npix)] for i in range(Npix)]
# # # np.save('map_T', map_T)
# # # print('Tmap saved')
# #
# default_emptiness(0, map_v) # for vmap in phase] for phase in map_v]
# default_emptiness(1, map_flux) # for fluxmap in phase] for phase in map_flux]
# map_v_mean = [[averie(map_v[i][j], weights=map_flux[i][j]) for j in range(Npix)] for i in range(Npix)] # for ln in range(2)] for phase in range(3)]
# np.save('map_v_mean', map_v_mean)
# print('vMap saved')
# #
# # mapv_a_sigma = np.array([[np.std(z) for z in y] for y in mapv_a])
# # mapv_b_sigma = np.array([[np.std(z) for z in y] for y in mapv_b])
# #
# # spectral_ratio = [0.5 * np.ones((Npix, Npix)) for i in range(3)]
# # for phase in range(3):
# #     im = ims[phase]
# #     for i in range(Npix):
# #         for j in range(Npix):
# #             flux_a = im[0][i][j]
# #             flux_b = im[1][i][j]
# #             if flux_a or flux_b:
# #                 if flux_a and flux_b and i < 128 and j < 128:
# #                     print(i, j, flux_a, flux_b, flux_a / (flux_a + flux_b))
# #                 spectral_ratio[phase][i][j] = flux_a / (flux_a + flux_b)
# # np.save('spectral_ratio', spectral_ratio)
# # print('Ratios saved')
# # print('Data saved')

f = h5py.File(fname, 'r')
print('Keys are ', f.keys())
img = f['camera_0']['image']

# plt.style.use('dark_background')
plt.set_cmap('magma')
phases = ['Cold', 'Warm', 'Hot']
imgs = [False for i in range(15)]

# # Draw in one screen
# blues = [False for i in range(3)]
# fig, axs = plt.subplots(ncols = 5, nrows = 3)
# tit = ['Img', 'Velocity in H-a', 'Velocity in H-b', 'H-a ratio', 'Temperature from Image', 'Img', 'Velocity in H-a', 'Velocity in H-b', 'H-a ratio', 'Temperature', 'Img', 'Velocity in H-a', 'Velocity in H-b', 'H-a ratio']
# imgs[0] = draw(axs[0][0], img)
# for phase in range(3):
#     fiv = 5 * phase
#     for j in range(1, 5):
#         tit[j + fiv] = phases[phase] + ' Gas ' + tit[j + fiv]
#     mini, maxi = clip8(ims[phase][0])
#     imgs[1 + fiv] = draw(axs[phase][1], ims[phase][0], alpha=0.5, cmap='Reds')
#     if mini and maxi:
#         blues[phase] = axs[phase][1].imshow(ims[phase][1], alpha=0.5, cmap='Blues',  interpolation='nearest', norm=colors.LogNorm(vmin=mini, vmax=maxi))
#         cbar = fig.colorbar(blues[phase], ax = axs[phase][1])
#     else:
#         blues[phase] = axs[phase][1].imshow(ims[phase][1], alpha=0.5, cmap='Blues',  interpolation='nearest', norm=colors.LogNorm())
#         cbar = fig.colorbar(blues[phase], ax = axs[phase][1])
#     cbar.minorticks_on()
#     imgs[2 + fiv] = draw(axs[phase][2], map_v_mean[phase][0], type='v')
#     imgs[3 + fiv] = draw(axs[phase][3], map_v_mean[phase][1], type='v')
#     imgs[4 + fiv] = draw(axs[phase][4], spectral_ratio[phase],type='ratio')
# imgs[5] = draw(axs[1][0], map_T)
# imgs[10] = draw(axs[2][0], im_T)
# axs = np.reshape(axs, 15)
# for i in range(15):
#     axs[i].set_title(tit[i])
# fig.show()

# Draw in multiple screens
phase = 0
# for phase in range(3):
fig, axs = plt.subplots(ncols = 3, nrows = 1)
img = [[row[j] for j in range(114,142)] for row in img[114:141]]
map_v_centre = [[row[j] for j in range(114,142)] for row in map_v_mean[114:141]]
dats = (img, map_v_mean, map_v_centre)
typs = ('log', 'v', 'v')
tit = ['Img', 'Velocity Img', 'Velocity Img at Centre']
for i in range(3):
    tit[i] = phases[phase] + ' Gas ' + tit[i]
    imgs[i] = draw(axs[i], dats[i], type=typs[i])
    axs[i].set_title(tit[i])
    axs[i].xaxis.set_major_formatter(mticker.FormatStrFormatter('%d kpc'))
    axs[i].yaxis.set_major_formatter(mticker.FormatStrFormatter('%d kpc'))
fig.show()

# # Old
# axs = np.reshape(axs, 15)
# imgs = (img, mapv_a_mean, mapv_b_mean, spectral_ratio, mapv_a_sigma, mapv_b_sigma)
# cmaps = ('magma', 'bwr', 'bwr', 'bwr', 'magma', 'magma')
#
# tit = ('Image', 'Mean velocity in H-a', 'Mean velocity in H-b', 'H-a ratio', 'StD of velocity in H-a', 'StD of velocity in H-b')
# ims = [[] for i in imgs]
# for i in range(len(imgs)):
#     if i == 0:
#         maxi = np.max(imgs[i])
#         ims[i] = axs[i].imshow(imgs[i], cmap=cmaps[i], interpolation='nearest', norm=colors.LogNorm(vmin=maxi/1e8, vmax = maxi))
#     elif i in (1, 2): # velocity
#         # peak = np.ptp(imgs[i])
#         peak = 1300
#         ims[i] = axs[i].imshow(imgs[i], cmap='bwr', interpolation='nearest', norm=colors.Normalize(vmin=-peak, vmax=peak))
#     elif i == 3: # H-a ratio
#         ims[i] = axs[i].imshow(imgs[i], cmap=cmaps[i], interpolation='nearest', norm=colors.LogNorm(vmin = 0.25))
#     else:
#         ims[i] = axs[i].imshow(imgs[i], cmap=cmaps[i], interpolation='nearest', norm=colors.LogNorm())
#     axs[i].set_title(tit[i])
#     cbar = fig.colorbar(ims[i], ax = axs[i])
#     cbar.minorticks_on()
# # plts[0].contour(imgs[0], alpha = 0.1)

input('Input to exit >>>')
