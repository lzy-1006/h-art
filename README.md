# README #

Galactic H-alpha/beta Line Radiative Transfer in an Octree Data Structure

Summer research @ UC Berkeley Astronomy, 2019

1. Cell-based Monte-Carlo numerical simulator of photon random walks in a galaxy.
2. Expanded legacy simulator in C, data processing & visualisation in Python.
3. Generated & processed ~15GB of photon data.

### Acknowledgement ###

Forked from Dr Xiangcheng Ma's OctreeRT code
